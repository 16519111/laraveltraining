<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@display');

Route::get('/register', 'AuthController@displayRegister');

Route::get('/welcome', 'AuthController@displayWelcome');
Route::post('/welcome', 'AuthController@getNameData')->name('name.store');