<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <title>Sanbercode Welcome</title>
</head>
<body> 
    <?php
        $firstName = $data["firstName"];
        $lastName = $data["lastName"];
        echo "<h1>SELAMAT DATANG $firstName $lastName!</h1>";
        echo "<br>";
        echo "<h3>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama</h3>";
    ?>
</body>
</html>

