<!DOCTYPE html>
<html>
<head>
    <title>Sanbercode Form</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Buat Akun Baru!</h1>
    <h3>Sign Up Form</h2>
    <form action="{{ route('name.store') }}" method="post">
        {{ csrf_field() }}
        <label>First name:</label><br><br>
        <input type="text" name="firstName"/><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastName"/><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="Male">Male</input><br>
        <input type="radio" name="gender" value="Female">Female</input><br>
        <input type="radio" name="gender" value="Other">Other</input><br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia</input><br>
        <input type="checkbox" name="language" value="English">English</input><br>
        <input type="checkbox" name="language" value="Other">Other</input><br><br>
        <label>Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up"/>
    </form>
</body>
</html>