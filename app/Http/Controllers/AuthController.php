<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function displayRegister() {
        return view('register');
    }

    function displayWelcome() {
        return view('welcome');
    }

    function getNameData(Request $request) {

        return view('welcome')->with('data',$request);
    }
}
